FROM alpine:edge

WORKDIR /app

# We include the known hosts here so we don't have to do this for every job we
# run.
RUN mkdir -p /root/.ssh
RUN chmod 700 /root/.ssh
COPY config/known_hosts /root/.ssh/known_hosts

RUN apk add --update git openssh bash git-lfs
RUN git config --global user.name 'GitLab Bot' && \
    git config --global user.email gitlab-bot@gitlab.com && \
    git lfs install

ADD . /app/
