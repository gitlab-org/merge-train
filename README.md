# Merge Train

![Multi track drifting](multi_track_drifting.png)

Merge Train is a project used to automatically merge changes from a source
repository into a target repository. Changes can be merged using `git merge`, or
by copying over all changes except for proprietary files such as the `ee/`
directory in GitLab. In both cases merge conflicts are handled automatically.

## Contributions

This project is owned by [Team Delivery](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/).  Please leverage their [issue
tracker](https://gitlab.com/gitlab-com/gl-infra/delivery) as necessary.

Also see [CONTRIBUTING.md](CONTRIBUTING.md)

## Strategies

There are two merge strategies provided by the Merge Train: a regular merge, and
a FOSS merge.

### Regular merge

A regular merge is the default approach, and uses `git merge` to merge changes
from the source repository into the target repository. Conflicts are resolved by
taking the version of the target repository.

### FOSS merge

A FOSS merge can be used to merge changes from a GitLab repository into a
repository that does not contain any proprietary code. This approach does not
use `git merge`, instead it applies all changes from the source repository
followed by removing any proprietary files and folders. To use this strategy,
pass the `foss` argument to the `merge-train` executable.

## Requirements

* Git
* Bash
* Push access to the target repository.
* Make (during development)

## Installation

Clone the repository:

    git@gitlab.com:gitlab-org/merge-train.git

Then you can run the merge train as follows to merge all CE `master` changes
into EE `master`:

    ./bin/merge-train

To merge changes from GitLab into a FOSS only repository, run the following
instead:

    SOURCE_PROJECT='gitlab-org/gitlab' TARGET_PROJECT='gitlab-org/gitlab-foss' ./bin/merge-train foss

This would merge all changes from gitlab-org/gitlab into gitlab-org/gitlab-foss,
and remove any proprietary code from the target repository.

### On macOS

macOS doesn't ship with the `realpath` program used by the `merge-train` script.
Install it via [Homebrew](https://brew.sh/):

    brew install coreutils

## Using in CI

Setting up Merge Train in CI is pretty straightforward, simply add the following
to your `.gitlab-ci.yml`:

```yaml
stages:
  # ... other stages ...

merge:master:
  image: registry.gitlab.com/gitlab-org/merge-train
  stage: merge
  only:
    refs:
      - schedules
    variables:
      - $SSH_PUBLIC_KEY
      - $SSH_PRIVATE_KEY
  script:
    - bash /app/bin/merge-train
  cache:
    paths:
      - source
      - target
    key: 'merge:master'
```

The script `merge-train` automatically clones (and caches) GitLab CE and EE
(configurable), merges changes from CE into EE using a time range, then pushes
the changes into EE.

The `merge-train` script requires the following variables to be set in CI:

1. `SSH_PUBLIC_KEY`
1. `SSH_PRIVATE_KEY`

The private and public key are located in 1Password in the "Release" vault, with the name `merge-train: GitLab EE security/master deploy key`

To change the source and target repositories, change the `SOURCE_PROJECT` and
`TARGET_PROJECT` variables, respectively.

In addition to the above, it is possible to specify source and target branches,
by supplying `SOURCE_BRANCH` and `TARGET_BRANCH` variables.

### Cache clean up

It is a [known issue](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20851) that caching and reusing a git clone of GitLab repository
causes the cache becoming too heavy. Thus, it is necessary to clean up the cache regularly. However, currently there is no CI setting to do it.
A mitigation method is to clean up the git clone before caching it again.

To enable the feature, when calling the `bin/merge-train` script, set the variable `CLEAN_UP_CACHE_PROBABILITY_PERCENTAGE` to between 1 and 100.
The git clone directories will be cleaned up randomly with the provided probability. In CI, the variable can be set at project level, job level
or when creating a new pipeline. If the variable is unset, cache clean up is disabled.

Example CI definition to set the variable at job level:

```yaml
merge:master:
  image: registry.gitlab.com/gitlab-org/merge-train
  stage: merge
  only:
    refs:
      - schedules
    variables:
      - $SSH_PUBLIC_KEY
      - $SSH_PRIVATE_KEY
  variables:
    - CLEAN_UP_CACHE_PROBABILITY_PERCENTAGE: 10
  script:
    - bash /app/bin/merge-train
  cache:
    paths:
      - source
      - target
    key: 'merge:master'
```

## Updating known hosts

The known GitLab.com hosts are baked into the Docker image. To update this list,
run the following:

    make config/known_hosts

You can then commit the changes as usual.

## Updating a project to use the merge-train deploy key

If the merge-train has permission errors accessing a project, it likely means that the deploy key needs to be added, to do this:

1. Go to the deploy keys settings page
1. Go to the tab "Privately accessible deploy keys"
1. Enable "GitLab FOSS sync"
1. Ensure that write access is enabled for the key
1. The key is now used
